#!/bin/bash
#DOW=$(date +%u)
if [ $# -eq 0 ] #se non ci sono parametri
then
  DOW=$(date +%u) #data standard
else
  DOW=$1 #data passata come parametro
fi

case "$DOW" in
"1")
sed -n 2,9p ./UniTS.org  | sed 's/\*\* //g' | sed 's/SCHEDULED: <2019-10-28 //g' | sed 's/+1w>//g'
    ;;
"2")
sed -n 11,12p ./UniTS.org  | sed 's/\*\* //g' | sed 's/SCHEDULED: <2019-10-29 //g' | sed 's/+1w>//g'
    ;;
"3")
sed -n 14,18p ./UniTS.org  | sed 's/\*\* //g' | sed 's/SCHEDULED: <2019-10-30 //g' | sed 's/+1w>//g'
    ;;
"4")
sed -n 20,24p ./UniTS.org  | sed 's/\*\* //g' | sed 's/SCHEDULED: <2019-10-31 //g' | sed 's/+1w>//g'
    ;;
"5")
sed -n 26,30p ./UniTS.org  | sed 's/\*\* //g' | sed 's/SCHEDULED: <2019-11-01 //g' | sed 's/+1w>//g'
    ;;
*)
curl en.wttr.in/trieste?nQ1F
    ;;
esac
